package nl.idwer.smurf.smurfenbos;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/data")
public class SmurfenbosRestApplication extends Application {
}
