# config.py
import os
basedir = os.path.abspath(os.path.dirname(__file__))
dbuser = 'pietpiraat'
dbpasswd = ''
dbname = 'workshopdb'
dbhost = 'localhost'

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or "heel-erg-ge-hijm"
    # SQLAlchemy config for sqlite
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db') or os.environ.get('DATABASE_URL')
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # SQLAlchemy config for Mariadb/Mysql
    # SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://' + dbuser + ':' + dbpasswd + '@' + dbhost + '/' + dbname
